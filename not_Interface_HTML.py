# Importación de Las librerias

from langchain.text_splitter import RecursiveCharacterTextSplitter #Libreria para Splitear la informacion
from langchain.embeddings import HuggingFaceEmbeddings #Librearia para crear los embeddings
from langchain.vectorstores import FAISS #libreria para crear la data
from langchain.chains.question_answering import load_qa_chain #Libreria para cargar el modelo LLM
from langchain import HuggingFaceHub  #Libreraia para usar los modelos de hugginface
from bs4 import BeautifulSoup  #Libreria para trabajar con html
import os #libreria para usar el token de hugginFace

# Configuración del token de la API de Hugging Face Hub
os.environ["HUGGINGFACEHUB_API_TOKEN"] = "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"

# Especificación de la ruta al archivo HTML que se va a procesar debe estar en la misma carpeta del proyecto
html_file_path = 'Documento-de-examen-Grupo1.html'

# Apertura del archivo HTML en modo lectura y lectura de su contenido
with open(html_file_path, 'r', encoding='utf-8') as html_file:
    html_content = html_file.read()

# Creación de un objeto BeautifulSoup para analizar el contenido HTML
soup = BeautifulSoup(html_content, 'html.parser')

# Extracción del texto del contenido HTML
text = soup.get_text()

# Configuración del divisor de texto basado en caracteres recursivos
text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=0)

# División del texto en fragmentos más pequeños
chunks = text_splitter.split_text(text)

# Creación de instancias de embeddings de Hugging Face
embeddings = HuggingFaceEmbeddings()

# Creación de la data a partir de los fragmentos de texto y los embeddings
knowledge_base = FAISS.from_texts(chunks, embeddings)

# Solicitud de una pregunta al usuario
pregunta = input("Ingrese la pregunta : ")

# Búsqueda de cosas relacionadas basada en la pregunta
docs = knowledge_base.similarity_search(pregunta)

# Creación del modelo de Hugging Face Hub modelo usado es google/flan-t5-base
llm = HuggingFaceHub(repo_id="google/flan-t5-base")

# Procesamiento de pregunta-respuesta
chain = load_qa_chain(llm, chain_type="stuff")

# Ejecución de la cadena de procesamiento para responder a la pregunta
print(chain.run(input_documents=docs, question=pregunta))
