# Importación de Las librerias

from PyPDF2 import PdfReader #Librearia para manejar PDF
from langchain.text_splitter import RecursiveCharacterTextSplitter #Libreria para Splitear la informacion
from langchain.embeddings import HuggingFaceEmbeddings #Librearia para crear los embeddings
from langchain.vectorstores import FAISS #libreria para crear la data
from langchain.chains.question_answering import load_qa_chain #Libreria para cargar el modelo LLM
from langchain import HuggingFaceHub  #Libreraia para usar los modelos de hugginface
import os #libreria para usar el token de hugginFace

# Configuración del token de la API de Hugging Face Hub
os.environ["HUGGINGFACEHUB_API_TOKEN"] = "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"

#Lee el documento pdf debe estar en la carpeta del proyecto
pdf_file_obj = open('Deberes Final (Python).pdf', 'rb')
pdf_reader = PdfReader(pdf_file_obj)

#Extrae el texto para poder usarlo
text = ""
for page in pdf_reader.pages:
    text += page.extract_text()

# Configuración del divisor de texto basado en caracteres recursivos
text_splitter = RecursiveCharacterTextSplitter(chunk_size=1000, chunk_overlap=0)

# División del texto en fragmentos más pequeños
chunks = text_splitter.split_text(text)

# Creación de instancias de embeddings de Hugging Face
embeddings = HuggingFaceEmbeddings()

# Creación de la data a partir de los fragmentos de texto y los embeddings
knowledge_base = FAISS.from_texts(chunks, embeddings)

# Solicitud de una pregunta al usuario
pregunta = input("Ingrese la pregunta : ")

# Búsqueda de cosas relacionadas basada en la pregunta
docs = knowledge_base.similarity_search(pregunta)

# Creación del modelo de Hugging Face Hub modelo usado es google/flan-t5-base
llm = HuggingFaceHub(repo_id="google/flan-t5-base")

# Procesamiento de pregunta-respuesta
chain = load_qa_chain(llm, chain_type="stuff")

# Ejecución de la cadena de procesamiento para responder a la pregunta
print(chain.run(input_documents=docs, question=pregunta))
